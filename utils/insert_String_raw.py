import sys

filename = sys.argv[1]

with open(filename, "r") as file:
    lines = file.readlines()


for i, line in enumerate(lines):
    if "const code = " in line:
        lines[i] = line.replace("const code = ", "const code = String.raw")

with open(filename, "w") as file:
    file.writelines(lines)