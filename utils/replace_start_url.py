import sys

filename = sys.argv[1]

with open(filename, "r") as file:
    lines = file.readlines()

for i, line in enumerate(lines):
    if '"start_url": "app.html"' in line:
        lines[i] = line.replace('"start_url": "app.html"', '"start_url": "./app.html"')
    if '"scope": "/"' in line:
        lines[i] = line.replace('"scope": "/"', '"scope": "./"')
        
with open(filename, "w") as file:
    file.writelines(lines)