# Interactive plotter for `pye2dpolar` data
Web-app for plotting the `.pkl` from `pye2dpolar`.  
It currenly supports `polar.pkl` and `flowfield.pkl` files. 

The app can be found here: https://kenloen.pages.windenergy.dtu.dk/polar_plotter_jupyterlite

## App Screenshots
![Screenshot Polar](https://gitlab.windenergy.dtu.dk/kenloen/polar_plotter_jupyterlite/uploads/a574a0d376b3945740f35794d828158c/Screenshot_from_2022-10-18_19-48-23.png)

![Screenshot Flow](https://gitlab.windenergy.dtu.dk/kenloen/polar_plotter_jupyterlite/uploads/b57966b513751f83e6086e00926d7bb5/Screenshot_from_2022-10-18_19-48-37.png)

## Installing the app
The app can be installed as a *Progressive-Web-App* (PWA). This has the advantages that the app can also be used offline as well as decreasing the boot-time.

**Installing PWA**
1. Open the web-app in either Chrome/Edge/Safari (Firefox will not work for this - it only works for the web-app)
2. At the end of the URL-address-bar an icon should appear which should show a computer screen with a downwards arrow and it should say "Install" when hovering the icon. Clicking the icon will install the app. It should not be possible to start the app from the start menu.