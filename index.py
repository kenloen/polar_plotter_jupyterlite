import numpy as np
import pickle
import xarray as xr
import holoviews as hv
import panel as pn
import hvplot.xarray
import hvplot.pandas
import io

# Holoviews settings
from holoviews import opts
_bk_size_opts = {"responsive": True, "max_width": 800}
opts.defaults(
    opts.Curve(framewise=True, padding=(0.05, 0.05), aspect=2.5),
    opts.Scatter(framewise=True, padding=(0.05, 0.05), aspect=2.5),
    opts.Points(framewise=True, padding=(0.05, 0.05), aspect=2.5),
    opts.QuadMesh(framewise=True, padding=(0.05, 0.05), aspect=2.5, colorbar=True),
    opts.Image(framewise=True, padding=(0.05, 0.05), aspect=2.5, colorbar=True),
    opts.NdOverlay(legend_position="right", framewise=True, padding=(0.05, 0.05), aspect=2.5),
    opts.Overlay(legend_position="right", framewise=True, padding=(0.05, 0.05), aspect=2.5),
    # Bokeh specific opts
    opts.Curve(backend="bokeh", tools=["hover", "vline"], **_bk_size_opts),
    opts.Scatter(backend="bokeh", tools=["hover", "vline"], **_bk_size_opts),
    opts.Points(backend="bokeh", tools=["hover", "vline"]),
    opts.QuadMesh(backend="bokeh", tools=["hover"], **_bk_size_opts),
    opts.Image(backend="bokeh", tools=["hover"], **_bk_size_opts),
    opts.NdOverlay(backend="bokeh", **_bk_size_opts, click_policy="hide"),
    opts.Overlay(backend="bokeh", **_bk_size_opts, click_policy="hide"),
)

# Load data functions
def load_dict(file_content):
    out = io.BytesIO()
    file.save(out)
    out.seek(0)
    return pickle.load(out)

def load_xarray(filename):
    pol_data = load_dict(filename)["gl1"]
    aoa = pol_data["aero_data"]["aoa"]
    s = pol_data["surface_data"][list(pol_data["surface_data"].keys())[0]]["s"]
    out = xr.Dataset(coords=dict(aoa=aoa, s=s))
    # Surface pol_data
    for key in list(pol_data["surface_data"][list(pol_data["surface_data"].keys())[0]].keys())[1:]:
        if key in ["s_stag"]: continue
        temp = np.empty((len(aoa), len(s)))
        for iaoa, val in enumerate(pol_data["surface_data"].values()):
            temp[iaoa, :] = val[key]
        out[key] = (("aoa", "s"), temp)
    # Aero-pol_data
    for key, pol_data in pol_data["aero_data"].items():
        if key in ["aoa"]: continue
        out[key] = ("aoa", pol_data)
    return out

def load_flow_dict(file_content):
    out = io.BytesIO()
    file_flow.save(out)
    out.seek(0)
    return pickle.load(out)

# Widgets 
# AoA pick
aoa1 = pn.widgets.DiscreteSlider(name="AoA 1")
aoa2 = pn.widgets.DiscreteSlider(name="AoA 2")
# Aero (Cl, Cd, ..)
x_aero_select = pn.widgets.Select(name="x")
y1_aero_select = pn.widgets.Select(name="y1")
y2_aero_select = pn.widgets.Select(name="y2")
# Surface (Cp, Cf, ..)
x_surf_select = pn.widgets.Select(name="x")
y1_surf_select = pn.widgets.Select(name="y1")
y2_surf_select = pn.widgets.Select(name="y2")

file = pn.widgets.FileInput(accept=".pkl")
wid_file = pn.WidgetBox("## Load file", "Select a *polar.pkl* file", file)
wid_aoa = pn.WidgetBox("## AoA-select", aoa1, aoa2)
wid_aero = pn.WidgetBox("## Aero-select", "(AoA, Cl, Cd, ..)", x_aero_select, y1_aero_select, y2_aero_select)
wid_surf = pn.WidgetBox("## Surface-select", "(s, x, Cp, Cf, ..)", x_surf_select, y1_surf_select, y2_surf_select)

# Flow
file_flow = pn.widgets.FileInput(accept=".pkl")
z1_flow = pn.widgets.Select(name="z1")
z2_flow = pn.widgets.Select(name="z2")
wid_file_flow = pn.WidgetBox("## Load file","Select a *flowfield.pkl* file","**Notice** that it takes a long time to load and update plots (see the spinner in the right corner)", file_flow)
wid_z = pn.WidgetBox("## z-select", z1_flow, z2_flow)


@pn.depends(pol_file=file)
def read_pol_data(pol_file):
    if pol_file is None:
        return 
    # Read file
    pol_data = load_xarray(pol_file)
    file.value = None
    # Add Cl/Cd
    pol_data["cl/cd"] = pol_data["cl"]/pol_data["cd"]
    # Update AoA sliders
    aoa1.options = list(pol_data["aoa"])
    aoa1.value = list(pol_data["aoa"])[0]
    aoa2.options = list(pol_data["aoa"])
    aoa2.value = list(pol_data["aoa"])[1]
    # Update aero
    aero_opt = ["aoa"]+[name for name, val in pol_data.data_vars.items() if len(val.coords) == 1]
    x_aero_select.options = aero_opt
    x_aero_select.value = "aoa"
    y1_aero_select.options = aero_opt
    y1_aero_select.value = "cl"
    y2_aero_select.options = aero_opt
    y2_aero_select.value = "cd"
    # Update surface
    surf_opt = ["s"]+[name for name, val in pol_data.data_vars.items() if len(val.coords) == 2]
    x_surf_select.options = surf_opt
    x_surf_select.value = "x"
    y1_surf_select.options = surf_opt
    y1_surf_select.value = "cp"
    y2_surf_select.options = surf_opt
    y2_surf_select.value = "cf"
    return plot_polar_data(pol_data)

def plot_polar_data(pol_data):
    if pol_data is None:
        return None
    idata = pol_data.interactive(width=800)

    idata_pol = idata.isel(s=0).to_dataframe()
    idata1 = idata.sel(aoa=aoa1).to_dataframe()
    idata2 = idata.sel(aoa=aoa2).to_dataframe()

    hvapp = (
        idata_pol.hvplot.line(x_aero_select, y1_aero_select).opts(line_color="black")
        *
        idata1.hvplot.scatter(x_aero_select, y1_aero_select).relabel("AoA 1")
        *
        idata2.hvplot.scatter(x_aero_select, y1_aero_select).relabel("AoA 2")
        +
        idata1.hvplot(x=x_surf_select, y=y1_surf_select).relabel("AoA 1")
        *
        idata2.hvplot(x=x_surf_select, y=y1_surf_select).relabel("AoA 2")
        +
        idata_pol.hvplot.line(x_aero_select, y2_aero_select).opts(line_color="black")
        *
        idata1.hvplot.scatter(x_aero_select, y2_aero_select).relabel("AoA 1")
        *
        idata2.hvplot.scatter(x_aero_select, y2_aero_select).relabel("AoA 2")
        +
        idata1.hvplot(x=x_surf_select, y=y2_surf_select).relabel("AoA 1")
        *
        idata2.hvplot(x=x_surf_select, y=y2_surf_select).relabel("AoA 2")
    ).cols(2)
    return hvapp.panel()

@pn.depends(flow_file=file_flow)
def read_flow_data(flow_file):
    # Load data
    if flow_file is None:
        return 
    flow_data = load_flow_dict(flow_file)
    # Add abs flow field
    flow_data["|u+v|"] = np.sqrt(flow_data["u"]**2+flow_data["v"]**2)
    # Set dropdown options
    z_names = [name for name in flow_data.keys() if not name in ["x", "y"]]
    z1_flow.options = z_names
    z1_flow.value = z_names[0]
    z2_flow.options = z_names
    z2_flow.value = z_names[1]

    quadmesh_opts = dict(
        data_aspect=1, aspect="equal", frame_height=400, frame_width=700, responsive=False, 
        xlim=(-0.5, 2), ylim=(-0.1, 0.2), cmap="viridis"
    )
    @pn.depends(z1=z1_flow)
    def update_z1(z1):
        return hv.QuadMesh((flow_data["x"], flow_data["y"], flow_data[z1]), ["x", "y"], z1).opts(**quadmesh_opts, clabel=z1)
    @pn.depends(z2=z2_flow)
    def update_z2(z2):
        return hv.QuadMesh((flow_data["x"], flow_data["y"], flow_data[z2]), ["x", "y"], z2).opts(**quadmesh_opts, clabel=z2)
    return pn.Column(update_z1, update_z2)



polar_tab = pn.Row(pn.WidgetBox(wid_file, wid_aoa, wid_aero, wid_surf), read_pol_data)
flow_tab = pn.Row(pn.WidgetBox(wid_file_flow, wid_z), read_flow_data)
tabs = pn.Tabs(("Polar", polar_tab), ("Flow", flow_tab))

app = pn.template.VanillaTemplate(title="pye2dpolar plotter", header_background="#2F3EEA", logo="dtu_logos/favicon.ico")
app.main.append(tabs)
app.servable()